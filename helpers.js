const colorRandomizer = () => {

	//This creates a random number which is then converted to string
	//but providing a number to the string method will then convert the number into a hex decimal
	//.floor() rounds of a number to the closest integer that is the largest equal or less than the number
	//5.95 = 5 if math.floor
	return Math.floor(Math.random()*16777215).toString(16)
}

export {colorRandomizer}