import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'

export default function MonthlyExpense({ExpenseData}){

	console.log(ExpenseData)
	
	const [ months, setMonths ] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
	const [ monthlyExpense, setMonthlyExpense ] = useState([])

	

	useEffect(()=>{
		let arrayJan = []
		let arrayFeb = []
		let arrayMar = []
		let arrayApr = []
		let arrayMay = []
		let arrayJun = []
		let arrayJul = []
		let arrayAug = []
		let arraySep = []
		let arrayOct = []
		let arrayNov = []
		let arrayDec = []

		setMonthlyExpense(ExpenseData.map(data => {
			console.log(data)
			if(moment(data.date).format('MMMM') === months[0] && data.category_type === "Expense"){
				arrayJan.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[1] && data.category_type === "Expense"){
				arrayFeb.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[2] && data.category_type === "Expense"){
				arrayMar.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[3] && data.category_type === "Expense"){
				arrayApr.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[4] && data.category_type === "Expense"){
				arrayMay.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[5] && data.category_type === "Expense"){
				arrayJun.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[6] && data.category_type === "Expense"){
				arrayJul.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[7] && data.category_type === "Expense"){
				arrayAug.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[8] && data.category_type === "Expense"){
				arraySep.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[9] && data.category_type === "Expense"){
				arrayOct.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[10] && data.category_type === "Expense"){
				arrayNov.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[11] && data.category_type === "Expense"){
				arrayDec.push(data.amount)
			}

		}))
		let totalJan = arrayJan.reduce((a,b) => a + b, 0)
		let totalFeb = arrayFeb.reduce((a,b) => a + b, 0)
		let totalMar = arrayMar.reduce((a,b) => a + b, 0)
		let totalApr = arrayApr.reduce((a,b) => a + b, 0)
		let totalMay = arrayMay.reduce((a,b) => a + b, 0)
		let totalJun = arrayJun.reduce((a,b) => a + b, 0)
		let totalJul = arrayJul.reduce((a,b) => a + b, 0)
		let totalAug = arrayAug.reduce((a,b) => a + b, 0)
		let totalSep = arraySep.reduce((a,b) => a + b, 0)
		let totalOct = arrayOct.reduce((a,b) => a + b, 0)
		let totalNov = arrayNov.reduce((a,b) => a + b, 0)
		let totalDec = arrayDec.reduce((a,b) => a + b, 0)

		let tempMonthlyExpense = []

		tempMonthlyExpense.splice(0, 0, totalJan)
		tempMonthlyExpense.splice(1, 0, totalFeb)
		tempMonthlyExpense.splice(2, 0, totalMar)
		tempMonthlyExpense.splice(3, 0, totalApr)
		tempMonthlyExpense.splice(4, 0, totalMay)
		tempMonthlyExpense.splice(5, 0, totalJun)
		tempMonthlyExpense.splice(6, 0, totalJul)
		tempMonthlyExpense.splice(7, 0, totalAug)
		tempMonthlyExpense.splice(8, 0, totalSep)
		tempMonthlyExpense.splice(9, 0, totalOct)
		tempMonthlyExpense.splice(10, 0, totalNov)
		tempMonthlyExpense.splice(11, 0, totalDec)
		console.log(tempMonthlyExpense)
		setMonthlyExpense(tempMonthlyExpense)
	}, [ExpenseData])

	


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Expense for the Year 2020',
				backgroundColor: 'purple',
				borderColor: 'black',
				borderWidth: 2,
				hoverBackgroundColor: 'blue',
				hoverBorderColor: 'black',
				data: monthlyExpense

			}
		]
	}

	return (

		<>
		<Bar data={data} />
		</>

	)


}