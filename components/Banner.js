//Jumbotron, row and col,
import Jumbotron from 'react-bootstrap/Jumbotron';

import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'

export default function Banner({data}){


	return(
			<Row className="mt-3">
				<Col>
					<Jumbotron className="bg-light">
						<h1>Zuitt Budget Tracker</h1>
						<p>Track your income and expenses here</p>
					</Jumbotron>
				</Col>
			</Row>
		)

}