import { useState, useEffect } from 'react'
import { Pie }  from 'react-chartjs-2'
import { Table, Row, Col, Tabs, Tab } from 'react-bootstrap'
import { colorRandomizer } from '../helpers'

export default function PieChart({recordsData}){
	
	
	const [ nameIncome, setNameIncome ] = useState([])
	const [ nameExpense, setNameExpense ] = useState([])
	const [ amountIncome, setAmountIncome ] = useState([])
	const [ amountExpense, setAmountExpense ] = useState([])
	const [ bgColors, setBgColors ] = useState([])

	console.log(recordsData)	

	useEffect(() => {

		let namesIncome = []
		let namesExpense = []

				recordsData.forEach(element => {

					if ((element.category_type === "Income") && (!namesIncome.find(name => name === element.category_name))){
						
						namesIncome.push(element.category_name)
					}
				})

					recordsData.forEach(element => {

						if ((element.category_type === "Expense") && (!namesExpense.find(name => name === element.category_name))){
							
							namesExpense.push(element.category_name)
						}
					})
		

		setNameIncome(namesIncome)
		setNameExpense(namesExpense)
	}, [recordsData])

	console.log(nameIncome)
	console.log(nameExpense)

	
	useEffect(()=>{

		

			setAmountIncome(nameIncome.map(name => {
				//this map will return acuumulated sales for each brand
				let total = 0//accumulator for total sales

				recordsData.forEach(element => {

					//forEach element with the matching brand:
					if((element.category_type === "Income") && (element.category_name === name)){

						//increment/accumulate the sales
						total = total + parseInt(element.amount)
					}
				})

				return total//return accumulated sales for brand
			}))
		 
			setAmountExpense(nameExpense.map(name => {
				//this map will return acuumulated sales for each brand
				let total = 0//accumulator for total sales

				recordsData.forEach(element => {

					//forEach element with the matching brand:
					if((element.category_type === "Expense") && (element.category_name === name)){

						//increment/accumulate the sales
						total = total + parseInt(element.amount)
					}
				})

				return total//return accumulated sales for brand
			}))
		
			//randomize the background colors per section of the pie
			setBgColors(nameIncome.map(() => `#${colorRandomizer()}`))
			setBgColors(nameExpense.map(() => `#${colorRandomizer()}`))
	}, [nameIncome, nameExpense])

		

		
		/*console.log(amount)*/
		/*console.log(bgColors)*/

		

	const dataIncome = {

		labels: nameIncome,//these will be the label to each part of the pie chart
		datasets: [
			{
				data: amountIncome,//this will be an array of number to be represented in our pie chart
				backgroundColor: bgColors,
				hoverBackgroundColor: []//will need an array of provided colors to show when we are hovering over a part of the Pie

			}
		]
	}

	const dataExpense = {

		labels: nameExpense,//these will be the label to each part of the pie chart
		datasets: [
			{
				data: amountExpense,//this will be an array of number to be represented in our pie chart
				backgroundColor: bgColors,
				hoverBackgroundColor: []//will need an array of provided colors to show when we are hovering over a part of the Pie

			}
		]
	}


	return (
		<>
		<h3 className="my-4 text-center">Category Breakdown</h3>
		<Tabs defaultActiveKey="income" id="categories" className="justify-content-lg-center mt-5">
      		<Tab eventKey="income" title="INCOME BREAKDOWN">
				<Pie data={dataIncome} />
			</Tab>

			<Tab eventKey="expense" title="EXPENSE BREAKDOWN">
				<Pie data={dataExpense} />
			</Tab>
		</Tabs>
		
		
		</>
	)
}