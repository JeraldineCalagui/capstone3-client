import { useEffect, useState } from 'react'

import { ListGroup, Row, Col, Table } from 'react-bootstrap'

export default function Income({data}){

	

	return (

		<>
	

		<Row className="justify-content-lg-center mt-5">
			<Col lg={6}>
				<Table>
					<tbody>
						<ListGroup>
							{data.map(expense => {

								if(expense.category_type === "Expense"){

									return (

										<ListGroup.Item key={expense._id} className="text-center">
											{expense.category_name}
										</ListGroup.Item>

									)
								}
							})
							}
						</ListGroup>
					</tbody>
				</Table>
			</Col>
		</Row>

		</>

	)
}