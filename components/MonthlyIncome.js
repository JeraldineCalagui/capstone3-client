import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'

export default function MonthlyIncome({IncomeData}){

	console.log(IncomeData)
	
	const [ months, setMonths ] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
	const [ monthlyIncome, setMonthlyIncome ] = useState([])

	

	useEffect(()=>{
		let arrayJan = []
		let arrayFeb = []
		let arrayMar = []
		let arrayApr = []
		let arrayMay = []
		let arrayJun = []
		let arrayJul = []
		let arrayAug = []
		let arraySep = []
		let arrayOct = []
		let arrayNov = []
		let arrayDec = []

		setMonthlyIncome(IncomeData.map(data => {
			console.log(data)
			if(moment(data.date).format('MMMM') === months[0] && data.category_type === "Income"){
				arrayJan.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[1] && data.category_type === "Income"){
				arrayFeb.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[2] && data.category_type === "Income"){
				arrayMar.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[3] && data.category_type === "Income"){
				arrayApr.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[4] && data.category_type === "Income"){
				arrayMay.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[5] && data.category_type === "Income"){
				arrayJun.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[6] && data.category_type === "Income"){
				arrayJul.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[7] && data.category_type === "Income"){
				arrayAug.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[8] && data.category_type === "Income"){
				arraySep.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[9] && data.category_type === "Income"){
				arrayOct.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[10] && data.category_type === "Income"){
				arrayNov.push(data.amount)
			} else if(moment(data.date).format('MMMM') === months[11] && data.category_type === "Income"){
				arrayDec.push(data.amount)
			}

		}))
		let totalJan = arrayJan.reduce((a,b) => a + b, 0)
		let totalFeb = arrayFeb.reduce((a,b) => a + b, 0)
		let totalMar = arrayMar.reduce((a,b) => a + b, 0)
		let totalApr = arrayApr.reduce((a,b) => a + b, 0)
		let totalMay = arrayMay.reduce((a,b) => a + b, 0)
		let totalJun = arrayJun.reduce((a,b) => a + b, 0)
		let totalJul = arrayJul.reduce((a,b) => a + b, 0)
		let totalAug = arrayAug.reduce((a,b) => a + b, 0)
		let totalSep = arraySep.reduce((a,b) => a + b, 0)
		let totalOct = arrayOct.reduce((a,b) => a + b, 0)
		let totalNov = arrayNov.reduce((a,b) => a + b, 0)
		let totalDec = arrayDec.reduce((a,b) => a + b, 0)

		let tempMonthlyIncome = []

		tempMonthlyIncome.splice(0, 0, totalJan)
		tempMonthlyIncome.splice(1, 0, totalFeb)
		tempMonthlyIncome.splice(2, 0, totalMar)
		tempMonthlyIncome.splice(3, 0, totalApr)
		tempMonthlyIncome.splice(4, 0, totalMay)
		tempMonthlyIncome.splice(5, 0, totalJun)
		tempMonthlyIncome.splice(6, 0, totalJul)
		tempMonthlyIncome.splice(7, 0, totalAug)
		tempMonthlyIncome.splice(8, 0, totalSep)
		tempMonthlyIncome.splice(9, 0, totalOct)
		tempMonthlyIncome.splice(10, 0, totalNov)
		tempMonthlyIncome.splice(11, 0, totalDec)
		console.log(tempMonthlyIncome)
		setMonthlyIncome(tempMonthlyIncome)
	}, [IncomeData])

	


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Income for the Year 2020',
				backgroundColor: 'purple',
				borderColor: 'black',
				borderWidth: 2,
				hoverBackgroundColor: 'blue',
				hoverBorderColor: 'black',
				data: monthlyIncome

			}
		]
	}

	return (

		<>
		<Bar data={data} />
		</>

	)


}