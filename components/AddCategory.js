import { useState, useEffect } from 'react'

import { Form, Button, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function addCategory(){

	const [ categoryName, setCategoryName ] = useState("")
	const [ categoryType, setCategoryType ] = useState("")
	const [ isActive, setIsActive ] = useState(false)


	useEffect(()=>{

		if(categoryName !== '' && categoryType !== ''){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [categoryName, categoryType])

	

	function addCategory(e){

		e.preventDefault()

		setCategoryName(categoryName)
		setCategoryType(categoryType)

		/*fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/category-exists`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				category_name: categoryName
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})*/

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/add-category`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category_type: categoryType,
				category_name: categoryName
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)
			if(data){

				setCategoryName("")
				setCategoryType("")

				Swal.fire({
					icon: 'success',
					title: 'Added Category',
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Could not add category',
				})
			}
			
		})		
	
	}
	

	return (
		<>
		<Row className="justify-content-lg-center mt-5">
			<Col lg={6}>
				<Form onSubmit={e => addCategory(e)}>
					<Form.Group controlId="categoryname">
						<Form.Label>Category Name:</Form.Label>
						<Form.Control type="string" placeholder="Please enter category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} />
					</Form.Group>
					<Form.Group controlId="categorytype">
						<Form.Label>Category Type:</Form.Label>
						<select className="form-control" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
							<option></option>
  							<option>Income</option>
  							<option>Expense</option>
						</select>
					</Form.Group>
					{
						isActive
						? <Button variant="info" type="submit" className="w-100 text-center d-flex justify-content-center">Add</Button>
						: <Button variant="secondary" type="submit" className="w-100 text-center d-flex justify-content-center" disabled>Add</Button>
					}
				</Form>
			</Col>
		</Row>
		</>
		
	)
}