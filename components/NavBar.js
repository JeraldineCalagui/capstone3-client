import { useContext, Fragment } from 'react'

import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'

import UserContext from '../UserContext'

export default function NavBar(){

	const { user } = useContext(UserContext)

	return (
		<Navbar bg="light" variant="light" expand="lg">
			<Link href="/">
				<a className="navbar-brand"><b>Budget Tracker</b></a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
				{
					user.id
					?
					<Fragment>
					<Link href="/categories">
						<a className="nav-link">Categories</a>
					</Link>
					<Link href="/records">
						<a className="nav-link">Records</a>
					</Link>
					<Link href="/monthlyIncome">
						<a className="nav-link">Monthly Income</a>
					</Link>
					<Link href="/monthlyExpense">
						<a className="nav-link">Monthly Expense</a>
					</Link>
					<Link href="#">
						<a className="nav-link">Trend</a>
					</Link>
					<Link href="/breakdown">
						<a className="nav-link">Breakdown</a>
					</Link>
					<NavDropdown title="Logout" id="collasible-nav-dropdown">
			        <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
			        <NavDropdown.Item href="/changepassword">Change Password</NavDropdown.Item>
			      	</NavDropdown>
					</Fragment>
					:
					<Fragment>
					<Link href="/login">
						<a className="nav-link">Login</a>
					</Link>
					<Link href="/register">
						<a className="nav-link">Register</a>
					</Link>
					</Fragment>
				}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}