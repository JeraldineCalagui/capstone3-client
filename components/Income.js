import { useEffect, useState } from 'react'

import { ListGroup, Row, Col, Table } from 'react-bootstrap'

export default function Income({data}){

	

	return (

		<>
	

		<Row className="justify-content-lg-center mt-5">
			<Col lg={6}>
				<Table>
					<tbody>
						<ListGroup>
							{data.map(income => {

								if(income.category_type === "Income"){

									return (

										<ListGroup.Item key={income._id} className="text-center">
											{income.category_name}
										</ListGroup.Item>

									)
								}
							})
							}
						</ListGroup>
					</tbody>
				</Table>
			</Col>
		</Row>

		</>

	)
}