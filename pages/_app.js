import { useState, useEffect, useContext, Fragment } from 'react'
import { Container } from 'react-bootstrap'

import NavBar from '../components/NavBar'


import { UserProvider } from "../UserContext"

import 'bootstrap/dist/css/bootstrap.min.css' 
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  
	const [user, setUser] = useState({

	email:null,

	})


	useEffect(() => {


		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data._id){
					setUser({
						id: data._id,
						firstname: data.firstname,
					})
			} else {
				setUser({
					id: null,
				})
			}
		})
	}, [])

	const unsetUser = () => {
		localStorage.clear()

		setUser({

			email:null,

		})
	}

  return (

  	<Fragment>
  		<UserProvider value={{user, setUser, unsetUser}}>
        <NavBar />	
        <Container>
	        <Component {...pageProps} />
        </Container>
        </UserProvider>
    </Fragment>

  )
}

export default MyApp
