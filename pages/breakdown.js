import { useState, useEffect } from 'react'

import PieChart from '../components/PieChart'





export default function breakdown(){

	const [ dataRecords, setDataRecords ] = useState([])
	

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/records`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			

			setDataRecords(data)

			
		})
	}, [])

	


	return(
		<>
		
		<PieChart recordsData={dataRecords}/>
		</>

	)
}