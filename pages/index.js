import Head from 'next/head'
import Banner from '../components/Banner'


export default function Home() {
  return (
    <>
      <Head>
        <title>Home | Budget Tracker</title>
      </Head>
      <Banner />
    </>
      
  )
}
