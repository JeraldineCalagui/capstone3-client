import { useState } from 'react'
import { Table, Row, Col, Tabs, Tab } from 'react-bootstrap'

import Head from 'next/head'

import Income from '../components/Income'
import Expense from '../components/Expense'
import AddCategory from '../components/AddCategory'

export default function categories(){

	const [ categoryData, setCategoryData ] = useState([])

	function refreshData(){

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`, {
						headers: {
							"Authorization": `Bearer ${localStorage.getItem('token')}`
						}
					})
					.then(res => res.json())
					.then(data => {
						
						setCategoryData(data.categories)
						
		})
	}
	

	return (
		<>
		<Head>
        	<title>Categories | Budget Tracker</title>
      	</Head>

      	<h3 className="text-center mt-5">CATEGORIES</h3>

      	<Tabs defaultActiveKey="addcategory" id="categories" className="justify-content-lg-center mt-5" onSelect={refreshData}>
      		<Tab eventKey="income" title="INCOME">
				<Income data={categoryData}/>
			</Tab>

			<Tab eventKey="expense" title="EXPENSE">
				<Expense data={categoryData}/>
			</Tab>

			<Tab eventKey="addcategory" title="ADD A CATEGORY">
				<AddCategory />
			</Tab>
			
		</Tabs>
		</>
	)
}