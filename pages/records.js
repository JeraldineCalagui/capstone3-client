import { useState, useEffect } from 'react'


import { Form, Button, Row, Col } from 'react-bootstrap'

import moment from 'moment'
import Router from 'next/router'

export default function records2(){

	const [ recordRows, setRecordRows ] = useState("")
	const [ search, setSearch ] = useState("")
	const [ filter, setFilter ] = useState("")

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/records`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(filter === "" || filter === "All"){

				if(search === ""){

					setRecordRows(data.slice(0).reverse().map(record => {
					/*setRecordRows(data.map(record => {*/
						let recordId = record._id
						console.log(record)
							return (

							<Row key={record._id} className="mb-3 border border-light bg-light py-3">
								<Col>
								<span>
									<h4>{record.description}</h4>
									<span>
									{
										record.category_type === "Income"
										? <b className="text-success">{record.category_type}</b>
										: <b className="text-danger">{record.category_type}</b>
									}
									</span>
									<span> <b>({record.category_name})</b></span>
									<p>{moment(record.date).format('MMMM Do YYYY')}</p>
								</span>
								
								</Col>
								{
									record.category_type === "Income"
									?
									<Col className="text-right">
										<p><b className="text-success">+ {record.amount}</b></p>
										<p className="text-success">{record.balance}</p>
											
									</Col>
									:
									<Col className="text-right">
										<p><b className="text-danger">- {record.amount}</b></p>
										<p className="text-danger">{record.balance}</p>
										
									</Col>
								}
							</Row>
							
							)
					}))
				} else {

					setRecordRows(data.slice(0).reverse().map(record => {
						
						let recordDescription = record.description
						if(recordDescription.match(search.trim())){

							return (

							<Row key={record._id} className="mb-3 border border-light bg-light py-3">
								<Col>
								<span>
									<h4>{record.description}</h4>
									<span>
									{
										record.category_type === "Income"
										? <b className="text-success">{record.category_type}</b>
										: <b className="text-danger">{record.category_type}</b>
									}
									</span>
									<span> <b>({record.category_name})</b></span>
									<p>{moment(record.date).format('MMMM Do YYYY')}</p>
								</span>
								
								</Col>
								{
									record.category_type === "Income"
									?
									<Col className="text-right">
										<p><b className="text-success">+ {record.amount}</b></p>
										<p className="text-success">{record.balance}</p>
									</Col>
									:
									<Col className="text-right">
										<p><b className="text-danger">- {record.amount}</b></p>
										<p className="text-danger">{record.balance}</p>
									</Col>
								}
								
							</Row>
							
							)
						}
					}))
				}


			} else if(filter === "Income"){
				setRecordRows(data.slice(0).reverse().map(record => {
					
					if(record.category_type === "Income"){

						let recordDescription = record.description
						if(recordDescription.match(search.trim())){


							return (

							
							<Row key={record._id} className="mb-3 border border-light bg-light py-3">
								<Col>
								<span>
									<h4>{record.description}</h4>
									<p><b className="text-success">{record.category_type}</b> <b>({record.category_name})</b></p>
									<p>{moment(record.date).format('MMMM Do YYYY')}</p>
								</span>
								
								</Col>
								<Col className="text-right">
									<p><b className="text-success">+ {record.amount}</b></p>
									<p className="text-success">{record.balance}</p>
								</Col>
							</Row>
							
							)

						}

					}

				}))
			} else {
				setRecordRows(data.slice(0).reverse().map(record => {
					
					if(record.category_type === "Expense"){

						let recordDescription = record.description
						if(recordDescription.match(search.trim())){

							return (

							
							<Row key={record._id} className="mb-3 border border-light bg-light py-3">
								<Col>
								<span>
									<h4>{record.description}</h4>
									<p><b className="text-danger">{record.category_type}</b> <b>({record.category_name})</b></p>
									<p>{moment(record.date).format('MMMM Do YYYY')}</p>
								</span>
								
								</Col>
								<Col className="text-right">
									<p><b className="text-danger">- {record.amount}</b></p>
									<p className="text-danger">{record.balance}</p>
								</Col>
							</Row>
							
							)
						}
					}

				}))
			}

		})
	}, [filter, search])

	/*function filterIncome(){


	}*/



	function goToAdd(){

		Router.push('/addRecords')
	}


	/*function deleteRecord(recordId){
		console.log(recordId)

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/${recordId}`, {
			method: "DELETE",
			headers: {
				"Content-Type" : "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

	}*/

	return (
		<>
		<Row className="my-4">
			<Col>
				<h3 className="my-4 text-center">RECORDS</h3>

			</Col>
		</Row>
		<Row className="my-2">

			<Col lg={2}>
				<Button variant="info" onClick={goToAdd} className="w-100 text-center d-flex justify-content-center">
				Add Record
				</Button>

			</Col>
			<Col lg={5}>
				<Form>
					<Form.Group>
						<Form.Control
							type="string"
							placeholder="Search Records"
							value={search}
							onChange={e => setSearch(e.target.value)}
							className="w-100 text-center d-flex justify-content-center"
						/>
					</Form.Group>
				</Form>

			</Col>
			<Col lg={5}>
				<Form>
				<Form.Group>
					<Form.Control as="select" placeholder="All" value={filter} onChange={e => setFilter(e.target.value)} className="w-100 text-center d-flex justify-content-center">
					  <option></option>
				      <option>All</option>
				      <option>Income</option>
				      <option>Expense</option>
				    </Form.Control>
			    </Form.Group>
			    </Form>

			</Col>
		</Row>
		{recordRows}
		</>
	)
}