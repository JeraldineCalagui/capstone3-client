import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'


import { Form, Button, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'

export default function changepassword(){

	const { user } = useContext(UserContext)

	const [ userId, setUserId ] = useState('')
	/*const [ oldPassword, setOldPassword ] = useState("")*/
	const [ newPassword, setNewPassword ] = useState("")

	const [ isActive, setIsActive ] = useState(false)

	useEffect(() => {

	setUserId(user.id)

	})

	useEffect(()=>{

		if(newPassword !== ''){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [newPassword])

	function changePassword(e){

		e.preventDefault()

		//update details of our course
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/${userId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: userId,
				password: newPassword,

			})
		}).then( res => {
			return res.json()
		}).then(data => {
			console.log(data)

			if(data === true ){
				Swal.fire({
					icon: 'success',
					title: 'Passwor Changed',
			
				})

				Router.push('/')
			} else {
			Swal.fire({
					icon: 'error',
					title: 'Could Not Change password. Contact developer.',
			
				})
			
			}

		})

	}


	return (
		<>
		<h3 className="justify-content-lg-center text-center mt-5">Change Password</h3>
		<Row className="justify-content-lg-center mt-5">
			<Col lg={6}>
				<Form onSubmit={(e) => changePassword(e)}>
					
					<Form.Group>
						<Form.Label>New Password</Form.Label>
						<Form.Control type="string" placeholder="Please Enter New Password" value={newPassword} onChange={e => setNewPassword(e.target.value)} />	
					</Form.Group>
					{
						isActive
						?
						<Button variant='primary' type='submit' className="w-100 text-center d-flex justify-content-center mb-5">Submit</Button>
						:
						<Button variant='secondary' type='submit' className="w-100 text-center d-flex justify-content-center mb-5" disabled>Submit</Button>
					}
				</Form>
			</Col>
		</Row>
		</>
	)


}