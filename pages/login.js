import { useState, useEffect, useContext } from 'react'

import { GoogleLogin } from 'react-google-login'


import UserContext from '../UserContext'

import { Form, Button, Row, Col } from 'react-bootstrap'

import Router from 'next/router'
import Swal from 'sweetalert2'
import Head from 'next/head'


export default function index(){

	const {user, setUser} = useContext(UserContext)

	const [ email, setEmail ] = useState('')
	const [ password, setPassword ] = useState('')

	const [ isActive, setIsActive ] = useState(false)

	useEffect(()=>{

		if(email !== '' && password !== ''){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password])

	function authenticate(e){
		
		e.preventDefault()


		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken){

				localStorage.setItem('token', data.accessToken)

				fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					/*console.log(data)
					console.log(data)*/
					setUser({
						id: data._id,
					})

					Swal.fire({
						icon: 'success',
						title: 'Login Successful',
				
					})

					
					Router.push('/')
				})
			} else {
				Swal.fire({
				icon: 'error',
				title: 'Authentication Failed',
				text: 'Email or password is incorrect'
			})
			}
		})
	}

	const authenticateGoogleToken = (response) => {

		


		const payload = {
			method: 'POST',
			headers: {	
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId,
				googleToken: response.accessToken
			})
		}
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/verify-google-id-token`, payload)
		.then(res => {
			return res.json()
		})
		.then(data => {
			
			if(typeof data.access !== 'undefined'){
				console.log('User has a valid token')
			
			localStorage.setItem('token', data.access)

			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/details`, {
				headers: {
					'Authorization':`Bearer ${data.access}`
				}
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				

				setUser({
					id: data._id,
					email: data.email,
					isAdmin: data.isAdmin
				})

				Router.push('/')
			})



			}else {

				if(data.error === 'google-auth-error'){
					Swal.fire({
						icon: 'error',
						title: 'Google Authentication Error',
						text: 'Google Authentication Procedure has failed, try again or contact web admin'
					})
				} else if ( data.error === 'login-type-error') {
					Swal.fire({
						icon: 'error',
						title: 'Login Type Error',
						text: 'You may have registered using a different login procedure. Try alternative login procedure'
					})
				}
			}
		})
	}

	return(
		<>
		<Head>
       		<title>Login | Budget Tracker</title>
      	</Head>

		<Row className="justify-content-lg-center mt-5" bg="light">
			<Col lg={6}>
				<Form onSubmit={(e) => authenticate(e)}>
					<Form.Group controlid='userEmail'>
						<Form.Label><b>Email Address</b></Form.Label>
						<Form.Control type='email' placeholder='Enter Email' value={email} onChange={(e) => setEmail(e.target.value)} />
					</Form.Group>
					<Form.Group controlid='userPassword'>
						<Form.Label><b>Password:</b></Form.Label>
						<Form.Control type='password' placeholder='Enter Password' value={password} onChange={(e) => setPassword(e.target.value)} />
					</Form.Group>
					{
						isActive
						?
						<Button variant='primary' type='submit' className="w-100 text-center d-flex justify-content-center mb-5">Submit</Button>
						:
						<Button variant='secondary' type='submit' className="w-100 text-center d-flex justify-content-center mb-5" disabled>Submit</Button>
					}
					
						<GoogleLogin
							clientId="125967190169-eekvv11mmujo9tk98a4hmbq7of2op2s7.apps.googleusercontent.com"	
							buttonText="Login using Google"
							cookiePolicy={'single_host_origin'}
							onSuccess={ authenticateGoogleToken}
							onFailuer={ authenticateGoogleToken}
							className="w-100 text-center d-flex justify-content-center"

						/>
				</Form>
			</Col>
		</Row>
		</>

	)

}

