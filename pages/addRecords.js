import { useState, useEffect } from 'react'

import { Form, Button, Row, Col } from 'react-bootstrap'

import Swal from 'sweetalert2'
import Router from 'next/router'

export default function addRecord(){
	
	//States for Modal
	const [ categories, setCategories ] = useState("")
	const [ categoryType, setCategoryType ] = useState("")
	const [ categoryName, setCategoryName ] = useState("")
	const [ amount, setAmount ] = useState("")
	const [ description, setDescription ] = useState("")
	const [ balance, setBalance ] = useState(0)

	const [ isActive, setIsActive ] = useState(false)

	const [ categoryNameData, setCategoryNameData ] = ("")
	const [ incomeData, setIncomeData ] = useState([])
	const [ expenseData, setExpenseData ] = useState([])


	useEffect(()=>{

		if(categoryName !== '' && categoryType !== '' && amount !== '' && description  !== ''){
			
			setIsActive(true)
			getBalance()

		} else {
			setIsActive(false)
		}

	}, [categoryName, categoryType, amount, description])


	//CategoryName useEffect
	useEffect(() => {


		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/categories`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			

			if(data.length >= 1){
				
				setIncomeData(data.map(income => {
					
					if(income.category_type === "Income"){

						
						return (

							<option key={income._id}>{income.category_name}</option>
						)
					}
				}))

				setExpenseData(data.map(expense => {
					if(expense.category_type === "Expense"){
						return (
							<option key={expense._id}>{expense.category_name}</option>
						)
					}
				}))
		}

		
		})
	}, [categoryType])

	console.log(incomeData)

	function getBalance(){

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/records`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)
			
			if(data.length === 0){


				if(categoryType === "Income"){
					setBalance(amount)
				} else {
					let currentBalance = 0
					let expenseAmount = currentBalance - amount
					setBalance(expenseAmount)
				}
				
			} else {
				let currentBalance = data[data.length-1].balance

				if(data.category_type === "Income"){

				currentBalance = currentBalance + parseInt(amount)
				setBalance(currentBalance)

				} else {

					currentBalance = currentBalance - parseInt(amount)
					setBalance(currentBalance)
				}
			}

			
		})
		

	}

	console.log(balance)
			

	function saveRecord(){


		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/add-record`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category_type: categoryType,
				category_name: categoryName,
				amount: amount,
            	description: description,
            	balance: balance
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)
			if(data){
				
				setCategoryName("")
				setCategoryType("")
				setAmount("")
				setDescription("")

				Swal.fire({
					icon: 'success',
					title: 'Added Category',
				})
			
				Router.push('/records')
				
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Could not add category',
				})
			}
			
		
		})
	}

	function cancelAdd(){
		Router.push('/records2')
	}



	return (
		<>
		<Row className="justify-content-lg-center mt-5">
			<Col lg={6} className="border">
				<Form className="mx-3 my-3">
	        		<Form.Group>
	        			<Form.Label>Category Type</Form.Label>
	        			<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
					 		<option></option>
				      		<option>Income</option>
				      		<option>Expense</option>
				    	</Form.Control> 	
	        		</Form.Group>
	        		<Form.Group>
	        			<Form.Label>Category Name</Form.Label>
	        			{
				   			categoryType.length > 0
				   			? categoryType === "Income"
				   				?
					   			<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)} >
						 		<option></option>
						 			{incomeData}
					    		</Form.Control>	
					    		:
					    		<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)} >
						 		<option></option>
						 			{expenseData}
					    		</Form.Control>	
				    		:
				    		<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)} disabled>
				    			
				    		</Form.Control>	

				   		}
	        			
	        		</Form.Group>
	        		<Form.Group>
	        			<Form.Label>Amount</Form.Label>
	        			<Form.Control type="number" placeholder="0" value={amount} onChange={e => setAmount(e.target.value)} />	
	        		</Form.Group>
	        		<Form.Group>
	        			<Form.Label>Description</Form.Label>
	        			<Form.Control type="string" placeholder="Enter description" value={description} onChange={e => setDescription(e.target.value)} />	
	        		</Form.Group>
	        		<Button variant="danger" onClick={cancelAdd}>Cancel</Button>
	        		{
	        			isActive
	        			? <Button variant="primary" className="ml-2" onClick={saveRecord}>Add Record</Button>
	        			: <Button variant="secondary" className="ml-2" disabled>Add Record</Button>
	        		}
	        	</Form>
	        	
			</Col>
		</Row>
		</>

	)
}
