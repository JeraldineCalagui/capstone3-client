import { useState, useEffect } from 'react'

import MonthlyIncome from '../components/MonthlyIncome'





export default function monthlyIncome(){

	const [ incomeData, setIncomeData ] = useState([])
	

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/records`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			

			setIncomeData(data)

			
		})
	}, [])

	


	return(
		<>
		<h3 className="my-4 text-center">Monthly Income in PHP</h3>
		<MonthlyIncome IncomeData={incomeData}/>
		</>

	)
}