import { useState, useEffect } from 'react'

import { Form, Button, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'

export default function register(){

	const [ firstName, setFirstName ] = useState("")
	const [ lastName, setLastName ] = useState("")
	const [ email, setEmail ] = useState("")
	const [ password1, setpassword1 ] = useState("")
	const [ password2, setpassword2 ] = useState("")
	const [ isActive, setIsActive ] = useState(false)

	useEffect(()=>{

		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 == password2)){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [firstName, lastName, email, password1, password2])

	function registerUser(e){

		e.preventDefault()

		setFirstName(firstName)
		setLastName(lastName)
		setEmail(email)
		setpassword1(password1)
		setIsActive(true)

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/email-exists`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if( data === false ){

			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/`, {
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					firstname: firstName,
					lastname: lastName,
					email: email,
					password: password1

				})
			})
			.then(res => res.json())
			.then(data => {

				setFirstName("")
				setLastName("")
				setEmail("")
				setpassword1("")

				Swal.fire({
					icon: 'success',
					title: 'Registered Successfully',
				})

				Router.push('/login')
			})

				
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Email Already Exists',
				})
			}
		})

			
	}

	return (
		<>
		<Row className="justify-content-lg-center mt-5">
			<Col lg={6}>
				<Form onSubmit={e => registerUser(e)}>
					<Form.Group controlId="firsname">
						<Form.Label><b>First Name:</b></Form.Label>
						<Form.Control type="string" placeholder="Please enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} />
					</Form.Group>
					<Form.Group controlId="lastname">
						<Form.Label><b>Last Name:</b></Form.Label>
						<Form.Control type="string" placeholder="Please enter last name" value={lastName} onChange={e => setLastName(e.target.value)} />
					</Form.Group>
					<Form.Group controlId="email">
						<Form.Label><b>Email Address:</b></Form.Label>
						<Form.Control type="email" placeholder="Please enter email Address" value={email} onChange={e => setEmail(e.target.value)} />
					</Form.Group>
					<Form.Group controlId="password1">
						<Form.Label><b>Password:</b></Form.Label>
						<Form.Control type="password" placeholder="Please enter password" value={password1} onChange={e => setpassword1(e.target.value)} />
					</Form.Group>
					<Form.Group controlId="password2">
						<Form.Label><b>Password:</b></Form.Label>
						<Form.Control type="password" placeholder="Please enter password" value={password2} onChange={e => setpassword2(e.target.value)} />
					</Form.Group>
					{
						isActive
						? <Button variant="primary" type="submit" className="w-100 text-center d-flex justify-content-center">Register</Button>
						: <Button variant="primary" type="submit" className="w-100 text-center d-flex justify-content-center" disabled>Register</Button>
					}
					
				</Form>
			</Col>
		</Row>
		</>
		
	)
}