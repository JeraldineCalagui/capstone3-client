import { useState, useEffect } from 'react'


import MonthlyExpense from '../components/MonthlyExpense'




export default function monthlyExpense(){

	const [ expenseData, setExpenseData ] = useState([])
	

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/users/records`, {

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			

			setExpenseData(data)

			
		})
	}, [])

	


	return(
		<>
		<h3 className="my-4 text-center">Monthly Expense in PHP</h3>
		<MonthlyExpense ExpenseData={expenseData}/>
		</>

	)
}